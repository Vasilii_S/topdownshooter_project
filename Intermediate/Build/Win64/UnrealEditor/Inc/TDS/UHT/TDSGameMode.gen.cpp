// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TDS/Game/TDSGameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTDSGameMode() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	TDS_API UClass* Z_Construct_UClass_ATDSGameMode();
	TDS_API UClass* Z_Construct_UClass_ATDSGameMode_NoRegister();
	UPackage* Z_Construct_UPackage__Script_TDS();
// End Cross Module References
	void ATDSGameMode::StaticRegisterNativesATDSGameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ATDSGameMode);
	UClass* Z_Construct_UClass_ATDSGameMode_NoRegister()
	{
		return ATDSGameMode::StaticClass();
	}
	struct Z_Construct_UClass_ATDSGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATDSGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_TDS,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "Game/TDSGameMode.h" },
		{ "ModuleRelativePath", "Game/TDSGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATDSGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATDSGameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ATDSGameMode_Statics::ClassParams = {
		&ATDSGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATDSGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATDSGameMode()
	{
		if (!Z_Registration_Info_UClass_ATDSGameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ATDSGameMode.OuterSingleton, Z_Construct_UClass_ATDSGameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ATDSGameMode.OuterSingleton;
	}
	template<> TDS_API UClass* StaticClass<ATDSGameMode>()
	{
		return ATDSGameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATDSGameMode);
	ATDSGameMode::~ATDSGameMode() {}
	struct Z_CompiledInDeferFile_FID_TDS_Source_TDS_Game_TDSGameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_TDS_Source_TDS_Game_TDSGameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ATDSGameMode, ATDSGameMode::StaticClass, TEXT("ATDSGameMode"), &Z_Registration_Info_UClass_ATDSGameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ATDSGameMode), 4009992965U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_TDS_Source_TDS_Game_TDSGameMode_h_352085510(TEXT("/Script/TDS"),
		Z_CompiledInDeferFile_FID_TDS_Source_TDS_Game_TDSGameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_TDS_Source_TDS_Game_TDSGameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
